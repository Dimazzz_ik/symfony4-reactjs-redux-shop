import React from "react";
import ProductCardList from "../components/ProductCardList";
import MainImage from "../components/MainImage";
import InCatalogSection from "../components/InCatalogSection";

class DeliveryAndPay extends React.Component {

    render() {
        return (
            <div>
                <MainImage />

                <div className="container text-center">
                    <h1 className="mt-3">Доставка и оплата</h1>

                    <h3>для города Воронеж:</h3>
                    <p>
                        доставка – бесплатно (с примеркой перед покупкой) <br/>
                        оплата – наличными (после примерки) / онлайн-переводом
                    </p>

                    <h3>Для других городов:</h3>
                    <p>
                        доставка - по тарифам Почты России или EMS (не более 500 р.) <br/>
                        оплата - наличными (после примерки) / онлайн-переводом <br/>
                        для других городов: онлайн-переводом или наложенным платежом. <br/>
                    </p>

                    <p>
                        Если остались вопросы, позвоните нам <a href="tel:+79010926940">+7 (901) 092 69 40</a>
                    </p>

                    <hr/>
                    <div>
                        <h2>Новинки</h2>
                    </div>


                    <ProductCardList itemCount={3}/>

                    <InCatalogSection />

                </div>
            </div>
        );
    }
}

export default DeliveryAndPay;