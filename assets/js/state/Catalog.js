import React from "react";
import ProductCardList from "../components/ProductCardList";

class Catalog extends React.Component {
    render() {
        return (
            <div className="container">
                <h1 className="mt-3 text-center">Каталог</h1>
                <ProductCardList />
            </div>
        );
    }
}

export default Catalog;