import React from "react";
import ProductCardList from '../components/ProductCardList';
import MainImage from '../components/MainImage';
import InCatalogSection from "../components/InCatalogSection";

class Home extends React.Component {
    render() {
        return (
            <div>
                <MainImage />
                <div className="container">
                    <div className="row">
                        <div className="col-10 offset-1" style={{padding: '30px 0', fontSize: '24px'}}>
                            Мы создаём платья опираясь на каноны стиля и последние тенденции моды.
                            Наши модели для девушек, которые чувствуют свой стиль, ценят качественные вещи и
                            хотят выглядеть особенно. <br />
                            Ищешь оригинальное платье для вечеринки, выпускного, корпоратива или
                            свадьбы? Выбери из готовых моделей в каталоге или закажи индивидуальный пошив.
                        </div>
                    </div>
                    <hr/>
                    <div>
                        <h2 className="text-center">Новинки</h2>
                    </div>

                    <ProductCardList itemCount={3}/>
                </div>
                <InCatalogSection />
            </div>
        );
    }
}

export default Home;