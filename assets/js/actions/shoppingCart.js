import * as types from '../const/ActionTypes'

function addToCartAction(product) {
    return {
        type: types.ADD_TO_CART,
        product
    };
}

export function addToCart(product) {
    return (dispatch) => {
        dispatch(addToCartAction(product));
    }
}

function removeFromCartAction(slug) {
    return {
        type: types.REMOVE_FROM_CART,
        slug
    };
}

export function removeFromCart(slug) {
    return (dispatch) => {
        dispatch(removeFromCartAction(slug));
    }
}

function clearShoppingCartAction() {
    return {
        type: types.CLEAR_CART
    };
}

export function clearShoppingCart() {
    return (dispatch) => {
        dispatch(clearShoppingCartAction());
    }
}
