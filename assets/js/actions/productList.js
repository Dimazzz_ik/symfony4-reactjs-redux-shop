import { getProductListUrl } from "../routes"
import * as types from '../const/ActionTypes'

function productListHasErrored(bool) {
    return {
        type: types.PRODUCT_LIST_HAS_ERRORED,
        hasErrored: bool
    };
}

function productListIsLoading(bool) {
    return {
        type: types.PRODUCT_LIST_IS_LOADING,
        isLoading: bool
    };
}

function productListFetchDataSuccess(items) {
    return {
        type: types.PRODUCT_LIST_FETCH_DATA_SUCCESS,
        items
    };
}


export function productListFetchData(count) {
    return (dispatch) => {
        dispatch(productListIsLoading(true));

        fetch(getProductListUrl, {
            method: 'POST',
            body: JSON.stringify({
                count: count,
            })
        })
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }

                dispatch(productListIsLoading(false));

                return response;
            })
            .then((response) => response.json())
            .then((data) => dispatch(productListFetchDataSuccess(data.productList)))
            .catch(() => dispatch(productListHasErrored(true)));
    };
}
