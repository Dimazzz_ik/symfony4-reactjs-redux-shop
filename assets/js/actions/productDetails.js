import { getProductUrl } from "../routes"
import * as types from '../const/ActionTypes'

function productHasErrored(bool) {
    return {
        type: types.PRODUCT_HAS_ERRORED,
        hasErrored: bool
    };
}

function productIsLoading(bool) {
    return {
        type: types.PRODUCT_IS_LOADING,
        isLoading: bool
    };
}

function productFetchDataSuccess(product) {
    return {
        type: types.PRODUCT_FETCH_DATA_SUCCESS,
        product,
    };
}


export function productFetchData(slug) {
    return (dispatch) => {
        dispatch(productIsLoading(true));

        fetch(getProductUrl, {
            method: 'POST',
            body: JSON.stringify({
                slug: slug,
            })
        })
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }

                dispatch(productIsLoading(false));

                return response;
            })
            .then((response) => response.json())
            .then((data) => {
                if (!data.success) {
                    window.location = '/404';
                    return;
                }
                dispatch(productFetchDataSuccess(data.product))
            })
            .catch(() => dispatch(productHasErrored(true)));
    };
}
