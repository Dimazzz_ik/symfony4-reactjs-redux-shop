import React from "react";
import { connect } from 'react-redux';
import { productListFetchData } from '../actions/productList';

import {Spinner} from "reactstrap";
import ProductCard from "./ProductCard";
import {addToCart} from "../actions/shoppingCart";

/**
 * itemCount - ограничение кол-ва в списке
 */

class ProductCardList extends React.Component {
    componentDidMount() {
        this.props.fetchData(this.props.itemCount || 0);
    }

    render() {
        const {productList, isLoading, hasErrored, addToCard} = this.props;

        if (isLoading) {
            return (
                <div className="text-center" style={{padding: '40px 0'}}>
                    <Spinner style={{width: '3rem', height: '3rem'}}/>
                </div>
            )
        }

        return (
            <div className="row">
                {productList.map(
                    (product) => {
                        const imageName = product.images.length ? product.images[0].imageName : '';

                        return (
                            <div className="col-12 col-md-4" key={product.id}>
                                <ProductCard
                                    id={product.id}
                                    name={product.name}
                                    price={product.price}
                                    imageName={imageName}
                                    slug={product.slug}
                                    action={() => addToCard({...product})}
                                >
                                </ProductCard>
                            </div>
                        )
                    }
                )}
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        productList: state.productList,
        hasErrored: state.productListHasErrored,
        isLoading: state.productListIsLoading
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchData: (count) => dispatch(productListFetchData(count)),
        addToCard: (product) => dispatch(addToCart(product))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ProductCardList);
