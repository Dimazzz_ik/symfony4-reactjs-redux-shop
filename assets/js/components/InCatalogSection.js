import React from 'react';
import {Button} from "reactstrap";
import {Link} from "react-router-dom";

const InCatalogSection = () => {
    return (
        <div className="col-12 text-center" style={{margin: '60px 0 30px'}}>
            <Button tag={Link} to='/catalog' outline color="secondary" style={{width: 250, padding: '15px 0', fontSize: 26}}>В каталог</Button>
        </div>
    )
};

export default InCatalogSection;