import React from "react";
import './style.scss';

class Index extends React.Component {
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = {
            isOpen: false
        };
    }
    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    render() {
        return (
            <footer className="footer">
                <div className="container">
                    <div className="row">
                        <div className="col-12 col-md-6 text-center">
                            Россия, Воронеж, +7 (901) 092 69 40
                            <br/>
                            email: cry.fashion.store@gmail.com
                        </div>

                        <div className="col-12 col-md-6 text-center">
                            Подпишись на нас в соцсетях: *значки-ссылки на инсту и вк*
                        </div>
                    </div>
                </div>
            </footer>
        );
    }
}

export default Index
