import React from 'react'
import image from '../../../img/devushki-v-platyah.jpg'

class MainImage extends React.Component{
    render() {
        return (
            <div>
                <img src={image} alt="Девушки в вечерних платьях" className='img-fluid' style={{width: '100%'}}/>
            </div>
        );
    }
}

export default MainImage;