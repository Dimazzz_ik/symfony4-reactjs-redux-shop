import React from "react";
import {NavLink as NavLinkReact} from "react-router-dom";
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavLink,
    NavItem,
} from 'reactstrap';

import './style.scss'
import ShoppingCart from "../ShoppingCart";

class Index extends React.Component {
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = {
            isOpen: false
        };
    }
    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    render() {
        return (
            <header>
                <Navbar color="light" light expand="md">
                    <div className="container">
                        <NavbarBrand tag={NavLinkReact} exact to="/" className="text-center">C R Y <br/> <span className="sub-title">Вечерние и коктельные платья</span></NavbarBrand>
                        <NavbarToggler onClick={this.toggle} />
                        <Collapse isOpen={this.state.isOpen} navbar>
                            <Nav className="ml-auto" navbar>
                                <NavItem>
                                    <NavLink tag={NavLinkReact} exact to="/" activeClassName="active">Главная</NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavLink tag={NavLinkReact} to="/catalog" activeClassName="active"> Каталог</NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavLink tag={NavLinkReact} to="/delivery-and-payment" activeClassName="active">Доставка и оплата</NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavLink tag={NavLinkReact} to="/about-us" activeClassName="active">О нас</NavLink>
                                </NavItem>
                                <NavItem>
                                    <ShoppingCart />
                                </NavItem>
                            </Nav>
                        </Collapse>
                    </div>
                </Navbar>
            </header>
        );
    }
}

export default Index
