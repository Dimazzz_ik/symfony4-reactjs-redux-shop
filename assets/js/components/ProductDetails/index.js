import React from "react";
import { connect } from 'react-redux';
import {productFetchData} from "../../actions/productDetails";

import {Spinner, FormGroup, Label, CustomInput, Input, Button} from "reactstrap"
import ProductDetailsCarousel from '../ProductDetailsCarousel'
import './style.scss'
import {addToCart} from "../../actions/shoppingCart";
import {shoppingCartProductList} from "../../reducers/shoppingCart";

class ProductDetails extends React.Component {

    componentDidMount() {
        this.props.fetchData(this.props.match.params.slug);
    }

    render() {
        const {product, isLoading, hasErrored, addToCard} = this.props;

        if (isLoading) {
            return (
                <div className="text-center" style={{padding: '40px 0'}}>
                    <Spinner style={{width: '3rem', height: '3rem'}}/>
                </div>
            )
        }

        const images = product.images ? product.images : [];

        return (
            <div>
                <div className="container">
                    <div className="row">
                        <div className="col-12 text-center">
                            <h1 style={{padding: '20px'}}>{product.name}</h1>
                        </div>
                        <div className="col-sm-7">
                            <ProductDetailsCarousel images={images} />
                        </div>
                        <div className="col-sm-5">
                            <div className="row">
                                <div className="col-6">
                                    <div className="decs-label">
                                        Цена:
                                    </div>
                                    <div className="decs-value">
                                        {product.price} руб
                                    </div>
                                </div>

                                <div className="col-6">
                                    <Button color="secondary" size="lg" block onClick={() => addToCard(product)}>Купить</Button>
                                </div>
                            </div>


{/*                            <div>
                                <FormGroup>
                                    <Label className="decs-label" for="sizeCheckbox">Размер:</Label>
                                    <CustomInput type="radio" id="sizeCheckbox" name="sizeCheckbox" label="S" />
                                    <CustomInput type="radio" id="sizeCheckbox2" name="sizeCheckbox" label="M" />
                                    <CustomInput type="radio" id="sizeCheckbox3"  name="sizeCheckbox" label="L" />
                                </FormGroup>
                            </div>


                            <div>
                                <FormGroup>
                                    <Label className="decs-label" for="productCount">Количество:</Label>
                                    <Input
                                        type="number"
                                        name="productCount"
                                        id="productCount"
                                        placeholder="Количество"
                                        defaultValue="1"
                                        style={{width: 100}}
                                    />
                                </FormGroup>
                            </div>*/}
                            <div>
                                <div className="decs-label mt-4">Информация о товаре:</div>
                                <div className="product-description">
                                    {product.description}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        product: state.product,
        hasErrored: state.productHasErrored,
        isLoading: state.productIsLoading
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchData: (slug) => dispatch(productFetchData(slug)),
        addToCard: (product) => dispatch(addToCart(product)),
        shoppingCartProductList: () => dispatch(shoppingCartProductList())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ProductDetails);
