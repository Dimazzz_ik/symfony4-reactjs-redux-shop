import React from "react";
import './style.scss'
import {removeFromCart, clearShoppingCart} from "../../actions/shoppingCart";
import connect from "react-redux/es/connect/connect";
import Dropdown from "reactstrap/es/Dropdown";
import DropdownToggle from "reactstrap/es/DropdownToggle";
import DropdownMenu from "reactstrap/es/DropdownMenu";
import Button from "reactstrap/es/Button";

import trashIcon from "../../../img/trash.svg";
import cartIcon from "../../../img/shopping_cart.svg";

class ShoppingCart extends React.Component {

    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = {
            isOpen: false
        }
    }

    toggle() {
        this.setState(prevState => ({
            isOpen: !prevState.isOpen
        }));
    }

    render() {
        const {productList, removeFromCart, clearShoppingCart} = this.props;

        return (
            <Dropdown direction="left" isOpen={this.state.isOpen} toggle={this.toggle}>
                <DropdownToggle color="link" className="drop-btn">
                    <img src={cartIcon} alt="" className="trash-icon"/> <span> Корзина {productList.length ? `(${productList.length})` : "" }</span>
                </DropdownToggle>
                <DropdownMenu className="p-3">
                    {!productList.length ? (
                        <h5 className="text-center m-2">Корзина пуста</h5>
                    ) : (
                        <div>
                            <h5 className="text-center mb-3">У вас в корзине</h5>

                            {productList.map(product => {
                                return (
                                    <div key={product.slug} className="mb-3 product">
                                        <div className="product__info mr-3"> {product.name}</div>
                                        <div className="product__price mr-1"> {product.price} Руб.</div>
                                        <div className="product__count mr-1"> {product.count || 1} шт.</div>
                                        <Button color="danger" size="sm" className="remove-btn float-right"
                                                onClick={() => removeFromCart(product.slug)}>
                                            <img src={trashIcon} alt=""/>
                                        </Button>
                                    </div>
                                )
                            })}
                            <hr/>
                            <div className="text-right">
                                Всего: {productList.reduce((prev, item) => item.price * item.count + prev, 0)} Руб
                            </div>
                            <hr/>
                            <div className="row">
                                <div className="col-6 text-left">
                                    <Button size="sm" color="secondary" onClick={clearShoppingCart}>Очистить</Button>
                                </div>
                                <div className="col-6 text-right">
                                    <Button size="sm" color="secondary" onClick={() => alert('все купил')}>Купить</Button>
                                </div>

                            </div>
                        </div>
                    )}
                </DropdownMenu>
            </Dropdown>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        productList: state.shoppingCartProductList
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        removeFromCart: (productId) => dispatch(removeFromCart(productId)),
        clearShoppingCart: () => dispatch(clearShoppingCart()),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ShoppingCart);


