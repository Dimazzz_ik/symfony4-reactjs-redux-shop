import React from 'react';
import {Card, CardImg, CardBody, CardTitle, CardSubtitle, Button} from 'reactstrap';
import {Link} from "react-router-dom";
import {imageProductPath} from '../../routes'
import './style.scss'

import noImage from '../../../img/noimage.jpg'

const Index = ({ id, name, price, imageName, slug, action}) => (
    <Card style={{marginTop: '30px'}}>
        <Link to={`/catalog/${slug}`}>
            <CardImg top width="100%" src={imageName ? (imageProductPath + imageName) : noImage} alt="Card image" />
        </Link>
        <CardBody>
            <CardTitle>{name}</CardTitle>
            <CardSubtitle>{price} Руб.</CardSubtitle>
            <Button onClick={action}>В корзину</Button>
        </CardBody>
    </Card>
);

export default Index;
