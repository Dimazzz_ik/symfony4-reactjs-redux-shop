export const imageProductPath = '/uploads/products_upload/';

export const getProductListUrl = '/ajax/get_product_list/';
export const getProductUrl = '/ajax/get_product/';

export const addToShoppingCartUrl = '/ajax/add_shopping_cart_product/';
export const removeFromShoppingCartUrl = '/ajax/remove_shopping_cart_product/';
export const getShoppingCartProductsUrl = '/ajax/get_shopping_cart_products/';
export const clearShoppingCartUrl = '/ajax/clear_shopping_cart/';