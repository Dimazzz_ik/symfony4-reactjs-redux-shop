import 'bootstrap/dist/css/bootstrap.css';
import '../css/app.css';

import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter as Router, Route, Switch, Redirect} from 'react-router-dom';

import { Provider } from 'react-redux';
import configureStore from './store/configureStore';

import Header from "./components/Header";
import Footer from "./components/Footer";
import ProductDetails from "./components/ProductDetails";

import Home from "./state/Home";
import AboutUs from "./state/AboutUs";
import Catalog from "./state/Catalog";
import DeliveryAndPay from "./state/DeliveryAndPay";
import NotFound from "./state/NotFound";

const store = configureStore();

class App extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <Router>
                    <div>
                        <Header />
                        <Switch>
                            <Route exact path="/" component={Home}/>
                            <Route exact path="/catalog" component={Catalog}/>
                            <Route exact path="/delivery-and-payment" component={DeliveryAndPay}/>
                            <Route exact path="/about-us" component={AboutUs}/>
                            <Route path="/catalog/:slug" component={ProductDetails}/>
                            <Route path='*' component={NotFound} />
                        </Switch>
                        <Footer />
                    </div>
                </Router>
            </Provider>
        );
    }
}

ReactDOM.render(<App />, document.getElementById('root'));
