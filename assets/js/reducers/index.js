import {combineReducers} from 'redux';
import {productList, productListHasErrored, productListIsLoading} from './productList';
import {product, productHasErrored, productIsLoading} from './productDetails';
import {shoppingCartProductList} from './shoppingCart';

export default combineReducers({
    productList,
    productListHasErrored,
    productListIsLoading,

    product,
    productHasErrored,
    productIsLoading,

    shoppingCartProductList,
});
