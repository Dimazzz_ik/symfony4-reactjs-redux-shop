import * as types from "../const/ActionTypes";

export function productHasErrored(state = false, action) {
    switch (action.type) {
        case types.PRODUCT_HAS_ERRORED:
            return action.hasErrored;

        default:
            return state;
    }
}

export function productIsLoading(state = false, action) {
    switch (action.type) {
        case types.PRODUCT_IS_LOADING:
            return action.isLoading;

        default:
            return state;
    }
}

export function product(state = [], action) {
    switch (action.type) {
        case types.PRODUCT_FETCH_DATA_SUCCESS:
            return action.product;

        default:
            return state;
    }
}
