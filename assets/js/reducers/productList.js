import * as types from '../const/ActionTypes'

export function productListHasErrored(state = false, action) {
    switch (action.type) {
        case types.PRODUCT_LIST_HAS_ERRORED:
            return action.hasErrored;

        default:
            return state;
    }
}

export function productListIsLoading(state = false, action) {
    switch (action.type) {
        case types.PRODUCT_LIST_IS_LOADING:
            return action.isLoading;

        default:
            return state;
    }
}

export function productList(state = [], action) {
    switch (action.type) {
        case types.PRODUCT_LIST_FETCH_DATA_SUCCESS:
            return action.items;

        default:
            return state;
    }
}
