import * as types from "../const/ActionTypes";

export function shoppingCartProductList(state = [], action) {
    switch (action.type) {
        case types.ADD_TO_CART:
            const product = action.product;
            const index = state.findIndex(item => item.slug === product.slug);
            if (index === -1) {
                product.count = 1;
                return [...state, product];
            }

            const stateTmp = [...state];
            stateTmp[index].count++;
            return stateTmp;
        case types.REMOVE_FROM_CART:
            return state.filter((item) => item.slug !== action.slug);
        case types.CLEAR_CART:
            return [];
        default:
            return state;
    }

}
