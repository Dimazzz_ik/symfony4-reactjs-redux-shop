*   php 7.2
    *   composer install
    *   php bin/console make:migration
    *   php bin/console doctrine:migrations:migrate
    *   php bin/console server:run  

*   yarn
    *   yarn run dev
    *   yarn run watch
    *   yarn run build
 