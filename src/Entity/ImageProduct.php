<?php
/**
 * Created by PhpStorm.
 * User: diman
 * Date: 04.02.2019
 * Time: 20:58
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * ImageProduct
 *
 * @ORM\Table(name="image_product")
 * @ORM\Entity
 * @Vich\Uploadable
 */
class ImageProduct
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="image_name", type="string", length=255)
     */
    private $imageName = "";

    /**
     * @Vich\UploadableField(mapping="image_product", fileNameProperty="image_name")
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $UpdatedAt;

    /**
     * @ORM\Column(name="order_number", type="integer", options={"default" : 100})
     * @var int
     */
    private $orderNumber = 100;

    public function __toString()
    {
        return $this->imageName ? $this->imageName : "";
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getImageName()
    {
        return $this->imageName;
    }

    /**
     * @param string $imageName
     */
    public function setImageName($imageName = ""): void
    {
        if (!$imageName){
            $this->imageName = "";
        }

        $this->imageName = $imageName;
    }

    /**
     * @return mixed
     */
    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    /**
     * @param mixed $imageFile
     */
    public function setImageFile(File $imageFile = null): void
    {
        $this->imageFile = $imageFile;

        if ($imageFile){
            $this->UpdatedAt = new \DateTime('now');
        }
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): ?\DateTime
    {
        return $this->UpdatedAt;
    }

    /**
     * @param \DateTime $UpdatedAt
     */
    public function setUpdatedAt(\DateTime $UpdatedAt): void
    {
        $this->UpdatedAt = $UpdatedAt;
    }

    /**
     * @return int
     */
    public function getOrderNumber(): ?int
    {
        return $this->orderNumber;
    }

    /**
     * @param int $orderNumber
     */
    public function setOrderNumber(int $orderNumber): void
    {
        $this->orderNumber = $orderNumber;
    }
}