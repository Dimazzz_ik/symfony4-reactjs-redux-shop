<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Product
 *
 * @ORM\Table(name="product")
 * @ORM\Entity
 */
class Product
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @Gedmo\Slug(fields={"name"}, updatable=true, separator="_")
     * @ORM\Column(name="slug", type="string", length=100, unique=true)
     */
    private $slug;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=true)
     */
    private $description;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="date_creation", type="datetime", nullable=false)
     */
    private $dateCreation;

    /**
     * @var int|0
     *
     * @ORM\Column(name="price", type="integer", nullable=false, options={"default" : 0})
     */
    private $price;

    /**
     * @var string|null
     *
     * @ORM\Column(name="seo_description", type="text", length=65535, nullable=true)
     */
    private $seoDescription;

    /**
     * @var string|null
     *
     * @ORM\Column(name="seo_title", type="string", length=100, nullable=true)
     */
    private $seoTitle;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $enabled = false;

    /**
     * @var ArrayCollection|ImageProduct
     *
     * @ORM\ManyToMany(targetEntity="ImageProduct", cascade={"persist", "remove"})
     * @ORM\OrderBy({"orderNumber" = "ASC"})
     * @ORM\JoinTable(name="product_image_product",
     *      joinColumns={@ORM\JoinColumn(name="product_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="image_id", referencedColumnName="id", unique=true)}
     *      )
     */
    private $images;

    public function __construct()
    {
        $this->images = new ArrayCollection();
        $this->dateCreation = new \DateTime('now');
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDateCreation(): ?\DateTimeInterface
    {
        return $this->dateCreation;
    }

    public function setDateCreation(?\DateTimeInterface $dateCreation): self
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    public function getSeoDescription(): ?string
    {
        return $this->seoDescription;
    }

    public function setSeoDescription(?string $seoDescription): self
    {
        $this->seoDescription = $seoDescription;

        return $this;
    }

    public function getSeoTitle(): ?string
    {
        return $this->seoTitle;
    }

    public function setSeoTitle(?string $seoTitle): self
    {
        $this->seoTitle = $seoTitle;

        return $this;
    }

    /**
     * @return int
     */
    public function getPrice(): ?int
    {
        return $this->price;
    }

    /**
     * @param int $price
     */
    public function setPrice(int $price): void
    {
        $this->price = $price;
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    /**
     * @param bool $enabled
     */
    public function setEnabled(bool $enabled): void
    {
        $this->enabled = $enabled;
    }

    /**
     * @return ImageProduct|ArrayCollection
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * @param ImageProduct|ArrayCollection $images
     */
    public function setImages($images): void
    {
        $this->images->clear();
        $this->images = $images;
    }

    /**
     * @param $image ImageProduct
     */
    public function addImageProduct($image)
    {
        if ($this->images->contains($image)) {
            return;
        }

        $this->images->add($image);
    }
    /**
     * @param ImageProduct $image
     */
    public function removeImageProduct($image)
    {
        if (!$this->images->contains($image)) {
            return;
        }
        $this->images->removeElement($image);
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }
}
