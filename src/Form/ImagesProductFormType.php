<?php

namespace App\Form;


use App\Entity\ImageProduct;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class ImagesProductFormType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('orderNumber', NumberType::class, array(
                'label' => 'admin.product.label.images.order',
                'translation_domain' => 'messages',
                'data' => 100,
            ))
            ->add('imageFile', VichImageType::class, array(
                'label' => 'admin.product.label.images.photo',
                'required'     => false,
                'allow_delete' => false,
                'mapped'       => 'image_product',
                'translation_domain' => 'messages'
            ))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => ImageProduct::class,
        ));
    }
}