<?php

namespace App\Controller;

use App\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    const SHOPPING_CART_SESSION_NAME = 'shoppingCartProductList';

    /**
     * @Route("/{reactRouting}", name="index", requirements={"reactRouting"="^(?!admin|ajax).+"}, defaults={"reactRouting": null})
     */
    public function index()
    {
        return $this->render('Default/index.html.twig', []);
    }

    /**
     * @Route("/ajax/get_product_list/", name="getProductList", methods={"POST"})
     *
     * @param Request $request
     * @return Object
     */
    public function getProductList(Request $request)
    {
        $data = json_decode($request->getContent(), true);

        if ($data && $data['count']) {
            $productList = $this->getDoctrine()->getRepository(Product::class)->findBy(
                ['enabled' => true],
                ['dateCreation' => 'ASC'],
                $data['count']
            );
        } else {
            $productList = $this->getDoctrine()->getRepository(Product::class)->findBy(
                ['enabled' => true]
            );
        }


        $result = [
            'success' => true,
            'productList' => $productList
        ];

        return $this->json($result);
    }

    /**
     * @Route("/ajax/get_product/", name="getProduct", methods={"POST"})
     *
     * @param Request $request
     * @return Object
     */
    public function getProduct(Request $request)
    {
        $data = json_decode($request->getContent(), true);

        if (!$data || !$data['slug']) {
            return $this->json([
                'error' => 'invalid arguments',
                'success' => false
            ]);
        }

        $product = $this->getDoctrine()->getRepository(Product::class)->findOneBy([
            'slug' => $data['slug'],
            'enabled' => true
        ]);

        if (!$product) {
            return $this->json([
                'error' => 'object not found',
                'success' => false
            ]);
        }

        $result = [
            'success' => true,
            'product' => $product
        ];

        return $this->json($result);
    }

    /**
     * Добавление в корзину
     * @Route("/ajax/add_shopping_cart_product/", name="addShoppingCart", methods={"POST"})
     *
     * @param Request $request
     * @return Object
     */
    public function addShoppingCart(Request $request)
    {
        $product = $this->getDoctrine()->getRepository(Product::class)->find($request->get('productId'));

        if (!$product) {
            return $this->json([
                'success' => false,
                'message' => 'product not found ' . $request->get('productId')
            ]);
        }

        $session = $request->getSession();
        $shoppingCartProducts = $session->get(self::SHOPPING_CART_SESSION_NAME);

        if (!$shoppingCartProducts) {
            $shoppingCartProducts = [$product->getId() => 1];
        } else if (isset($shoppingCartProducts[$product->getId()])) {
            $shoppingCartProducts[$product->getId()]++;
        } else {
            $shoppingCartProducts += [$product->getId() => 1];
        }

        $session->set(self::SHOPPING_CART_SESSION_NAME, $shoppingCartProducts);

        return $this->json([
            'success' => true
        ]);
    }

    /**
     * Удаляем из корзины
     * @Route("/ajax/remove_shopping_cart_product/", name="removeShoppingCart", methods={"POST"})
     *
     * @param Request $request
     * @return Object
     */
    public function removeShoppingCart(Request $request)
    {
        $productId = $request->get('productId');

        if (!$productId) {
            return $this->json([
                'success' => false,
                'message' => 'Неверно заданы параметры'
            ]);
        }

        $session = $request->getSession();
        $cardProducts = $session->get(self::SHOPPING_CART_SESSION_NAME);

        if (!$cardProducts || !isset($cardProducts[$productId])) {
            return $this->json([
                'success' => false,
                'message' => 'Товара нет в корзине'
            ]);
        }

        unset($cardProducts[$productId]);

        $session->set(self::SHOPPING_CART_SESSION_NAME, $cardProducts);

        return $this->json([
            'success' => true
        ]);
    }

    /**
     * Очищаем корзину
     * @Route("/ajax/clear_shopping_cart/", name="getShoppingCartProducts", methods={"POST"})
     *
     * @param Request $request
     * @return Object
     */
    public function clearShoppingCart(Request $request)
    {
        $session = $request->getSession();
        $session->set(self::SHOPPING_CART_SESSION_NAME, []);

        return $this->json([
            'success' => true
        ]);
    }

    /**
     * Получаем список продуктов
     * @Route("/ajax/get_shopping_cart_products/", name="getShoppingCartProducts", methods={"POST"})
     *
     * @param Request $request
     * @return Object
     */
    public function getShoppingCartProducts(Request $request)
    {
        $session = $request->getSession();

        $cardProducts = $session->get(self::SHOPPING_CART_SESSION_NAME);
        if (!$cardProducts){
            return $this->json([
                'products' => [],
                'total' => 0
            ]);
        }

        $total = 0;
        $products = [];

        foreach ($cardProducts as $key => $count){
            $product = $this->getDoctrine()->getRepository(Product::class)->find($key);
            if (!$product){
                continue;
            }
            $product->count = $count;
            $product->total = $count * $product->getPrice();
            $products += [$key => $product];
            $total += $product->total;
        }

        return $this->json([
            'products' => $products,
            'total' => $total
        ]);
    }
}